# ServiceNowRFC module

Simple NodeJS API and CLI to open and close ServiceNow RFCs.


## Usage

```js

// api
import from "servicenowrfc";

rfc = require("servicenowrfc");


rfc.open(


)
.then (


)


rfc.getID(


)
.then (


)


rfc.close(


)
.then (


)

// package.json & cli

"scripts": {
    "deploy:production": "rfc open && deploy-scripts.sh && rfc close completed || rfc close canceled",
},

```


## ServiceNow Spec Notes

### rfc.open
API call to ServiceNow to open a Standard RFC.

**Returns promise with:**

1. RFC ID

**Requires an object with the following attributes:**

1. RFC Template:
    - Defaults to **create umbrella standard rfc**
1. Short Description:
    - Git project name + Merge Commit Title
1. Description (JSON)
    - Commit contributors
    - Full Commit Message
    - Version
    - Build history
    - Commit hash
1. Service (one or more of)
    - default service for Doug's team
    - a metric project
    - the project service
1. Configuration
    - Future concern - cmdb stuff
1. Assignee
    - Merge requester

### rfc.close

Changes existing RFC State to canceled or completed.

**Returns promise with:**

1. updated status (completed || canceled)

**Requires following arguments:**

1. RFC ID[]
2. newStatus


### rfc.getID

Finds RFC ID Numbers matching provided information by querying ServiceNow's API.


**Returns promise with:**

1. Promise with an array of matched IDs

**Requires following arguments:**

1. A single object with as many of the required rfc.open attributes required to make the match.

**libs**

1. may use https://www.npmjs.com/package/patrun
